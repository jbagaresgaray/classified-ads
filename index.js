"use strict";

process.env.TZ = "UTC";
require("dotenv").config();

var env = process.env.NODE_ENV || "development";
process.env.NODE_ENV = env;

var application = require("./config/application"),
  express = require("express"),
  bunyan = require("bunyan"),
  mongoose = require("mongoose"),
  ejwt = require("express-jwt"),
  jwt = require("jsonwebtoken"),
  passport = require("passport"),
  Database = require("./app/utils/database").Database,
  config = require("./config/config"),
  db = new Database(mongoose, config),
  log = bunyan.createLogger({
    name: config.app_name
  }),
  app = express();

require(application.utils + "helper")(db, app, log);
require(application.utils + "loadschema")(mongoose);
require(application.config + "express")(app, passport, config, ejwt);

// ===================== ROUTES ===================== //
require(application.routes + "about")(app, config);
require(application.routes + "business")(app, config);
require(application.routes + "cuisines")(app, config);
require(application.routes + "events")(app, config);
require(application.routes + "homephotos")(app, config);
require(application.routes + "industry")(app, config);
require(application.routes + "property")(app, config);
require(application.routes + "restaurant")(app, config);
require(application.routes + "specials")(app, config);
require(application.routes + "users")(app, config);
require(application.routes + "upload")(app, config);

// ===================== DOCS ===================== //
require(application.routes + "docs")(app);
require(application.routes + "/")(app);

module.exports = app;
