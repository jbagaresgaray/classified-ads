module.exports = function (grunt) {
    grunt.initConfig({
	  mongobackup: {
	    options: {
	      host : 'localhost',
	      port: '27017',
	      db : 'ClassifiedAds', 
	      dump:{
	        out : './dump',
	      },    
	      restore:{
	        path : './dump/ClassifiedAds',          
	        drop : true
	      }
	    }  
	  }
	});

	grunt.loadNpmTasks('grunt-mongo-backup');
}