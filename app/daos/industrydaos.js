'use strict';

var async = require('async');
var mongoose = require('mongoose');
var industry = mongoose.model('Industry');

// Create
exports.createIndustry = function createIndustry(data, next) {
    industry.create(data, next);
};

//Read
exports.getIndustryByID = function getIndustryByID(id, next) {
    industry.findById(id,next);
};

exports.getIndustry = function getIndustry(next) {
    industry.find(next);
};

// Update
exports.updateIndustry = function updateIndustry(id, data, next) {
    industry.update(id, data, next);
};

//Delete
exports.deleteIndustry = function deleteIndustry(id, next) {
    industry.findByIdAndRemove(id, next);
};
