'use strict';

var async = require('async');
var mongoose = require('mongoose');
var restaurant = mongoose.model('Restaurant');

// Create
exports.createRestaurant = function createRestaurant(data, next) {
    restaurant.create(data, next);
};

//Read
exports.getRestaurantByID = function getRestaurantByID(id, next) {
    restaurant.find({
        "_id": id
    },next);
};

exports.getRestaurant = function getRestaurant(next) {
    restaurant.find(next);
};

// Update
exports.updateRestaurant = function updateRestaurant(id, data, next) {
    restaurant.update(id, data, next);
};

//Delete
exports.deleteRestaurant = function deleteRestaurant(id, next) {
    restaurant.remove({
        "_id": id
    }, next);
};
