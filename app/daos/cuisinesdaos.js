'use strict';

var async = require('async');
var mongoose = require('mongoose');
var cuisines = mongoose.model('Cuisines');

// Create
exports.createCuisines = function createCuisines(data, next) {
    cuisines.create(data, next);
};

//Read
exports.getCuisinesById = function getCuisinesById(id, next) {
    cuisines.findById(id, next);
};

exports.getCuisines = function getCuisines(next) {
    cuisines.find(next);
};

// Update
exports.updateCuisines = function updateCuisines(id, data, next) {
    cuisines.update(id, data, next);
};

//Delete
exports.deleteCuisines = function deleteCuisines(id, next) {
    cuisines.remove({
        "_id": id
    }, next);
};
