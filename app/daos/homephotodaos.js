'use strict';

var async = require('async');
var mongoose = require('mongoose');
var homephoto = mongoose.model('HomePhotos');

// Create
exports.createHomePhoto = function createHomePhoto(data, next) {
    homephoto.create(data, next);
};

//Read
exports.getHomePhotosById = function getHomePhotosById(id, next) {
    homephoto.find({
        "_id": id
    },next);
};

exports.getHomePhoto = function getHomePhoto(next) {
    homephoto.find(next);
};

// Update
exports.updateHomePhoto = function updateHomePhoto(id, data, next) {
    homephoto.update(id, data, next);
};

//Delete
exports.deleteHomePhoto = function deleteHomePhoto(id, next) {
    homephoto.remove({
        "_id": id
    }, next);
};
