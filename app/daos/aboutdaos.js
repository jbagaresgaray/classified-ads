'use strict';

var async = require('async');
var mongoose = require('mongoose');
var about = mongoose.model('About');

// Create
exports.createAbout = function createAbout(data, next) {
    about.findOne({
        '_id': data._id
    }, function(err, resp) {
    	console.log(resp);
    	if(!err){
    		if(resp != null){
    			about.update(data._id, data, next);
    		}else{
    			about.create(data,next);
    		}
    	}
    });
}

//Read
exports.getAbout = function getAbout(next) {
    about.find(next);
}

// Update
exports.updateAbout = function updateAbout(id, data, next) {
    about.update(id, data, next);
}

//Delete
exports.deleteAbout = function deleteAbout(id, next) {
    console.log('id: ', id);
    about.remove({
        "_id": id
    }, next);
}
