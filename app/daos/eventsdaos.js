'use strict';

var async = require('async');
var mongoose = require('mongoose');
var events = mongoose.model('Events');

// Create
exports.createEvents = function createEvents(data, next) {
    events.create(data, next);
};

//Read
exports.getEventsById = function getEventsById(id, next) {
    events.find({
        "_id": id
    },next);
};

exports.getEvents = function getEvents(next) {
    events.find(next);
};

// Update
exports.updateEvents = function updateEvents(id, data, next) {
    events.update(id, data, next);
};

//Delete
exports.deleteEvents = function deleteEvents(id, next) {
    events.remove({
        "_id": id
    }, next);
};
