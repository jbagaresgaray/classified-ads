'use strict';

var async = require('async');
var mongoose = require('mongoose');
var specials = mongoose.model('Specials');

// Create
exports.createSpecials = function createSpecials(data, next) {
    specials.create(data, next);
};

//Read
exports.getSpecialsByID = function getSpecialsByID(id, next) {
    specials.find({
        "_id": id
    },next);
};

exports.getSpecials = function getSpecials(next) {
    specials.find(next);
};

// Update
exports.updateSpecials = function updateSpecials(id, data, next) {
    specials.update(id, data, next);
};

//Delete
exports.deleteSpecials = function deleteSpecials(id, next) {
    specials.remove({
        "_id": id
    }, next);
};
