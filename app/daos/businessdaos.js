'use strict';

var async = require('async');
var mongoose = require('mongoose');
var business = mongoose.model('Business');

// Create
exports.createBusiness = function createBusiness(data, next) {
    business.create(data, next);
};

//Read
exports.getBusinessById = function getBusinessById(id, next) {
    business.findById(id, next);
};

exports.getBusiness = function getBusiness(next) {
    business.find(next);
};

// Update
exports.updateBusiness = function updateBusiness(id, data, next) {
    business.findByIdAndUpdate(id, data, next);
};

//Delete
exports.deleteBusiness = function deleteBusiness(id, next) {
    business.remove({
        "_id": id
    }, next);
};
