'use strict';

var async = require('async');
var mongoose = require('mongoose');
var property = mongoose.model('Property');

// Create
exports.createProperty = function createProperty(data, next) {
    property.create(data, next);
};

//Read
exports.getPropertyByID = function getPropertyByID(id, next) {
    property.find({
        "_id": id
    },next);
};

exports.getProperty = function getProperty(next) {
    property.find(next);
};

// Update
exports.updateProperty = function updateProperty(id, data, next) {
    property.update(id, data, next);
};

//Delete
exports.deleteProperty = function deleteProperty(id, next) {
    property.remove({
        "_id": id
    }, next);
};
