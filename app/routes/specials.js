'use strict';

var specialsCtrl = require('../controllers/specialcontroller');

module.exports = function(app, config) {

    app.route(config.api_version + '/specials/:id').get(specialsCtrl.getSpecialsByID)
    app.route(config.api_version + '/specials').get(specialsCtrl.getSpecials)
    app.route(config.api_version + '/specials').post(specialsCtrl.createSpecials)
    app.route(config.api_version + '/specials/:id').put(specialsCtrl.updateSpecials)
    app.route(config.api_version + '/specials/:id').delete(specialsCtrl.deleteSpecials);

};
