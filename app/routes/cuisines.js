'use strict';

var cuisinesCtrl = require('../controllers/cuisinescontroller');

module.exports = function(app, config) {

    app.route(config.api_version + '/cuisines/:id').get(cuisinesCtrl.getCuisinesById)
    app.route(config.api_version + '/cuisines').get(cuisinesCtrl.getCuisines)
    app.route(config.api_version + '/cuisines').post(cuisinesCtrl.createCuisines)
    app.route(config.api_version + '/cuisines/:id').put(cuisinesCtrl.updateCuisines)
    app.route(config.api_version + '/cuisines/:id').delete(cuisinesCtrl.deleteCuisines);

};
