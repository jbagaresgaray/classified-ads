'use strict';

var aboutCtrl = require('../controllers/aboutcontroller');

module.exports = function(app, config) {

    app.route(config.api_version + '/about').get(aboutCtrl.getAbout);
    app.route(config.api_version + '/about').post(aboutCtrl.createAbout);
    app.route(config.api_version + '/about/:id').put(aboutCtrl.updateAbout);
    app.route(config.api_version + '/about/:id').delete(aboutCtrl.deleteAbout);

};
