'use strict';

var businessCtrl = require('../controllers/businesscontroller');

module.exports = function(app, config) {

    app.route(config.api_version + '/business/:id').get(businessCtrl.getBusinessById);
    app.route(config.api_version + '/business').get(businessCtrl.getBusiness);
    app.route(config.api_version + '/business').post(businessCtrl.createBusiness);
    app.route(config.api_version + '/business/:id').put(businessCtrl.updateBusiness);
    app.route(config.api_version + '/business/:id').delete(businessCtrl.deleteBusiness);

};
