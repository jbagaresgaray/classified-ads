'use strict';

module.exports = function(app) {

    app.route('/login').get(function(req, res) {
        res.render('login');
    });

    app.route('/home').get(function(req, res) {
        res.render('home');
    });

    app.route('/').get(function(req, res) {
        res.render('index');
        // res.render('home');
    });

    app.route('/logout').post(function(req, res) {
        req.logOut();
        res.status(200).end();
    });
};
