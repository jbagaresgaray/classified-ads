'use strict';

var eventCtrl = require('../controllers/eventscontroller');

module.exports = function(app, config) {

    app.route(config.api_version + '/events').get(eventCtrl.getEvents);
    app.route(config.api_version + '/events').post(eventCtrl.createEvents);
    app.route(config.api_version + '/events/:id').get(eventCtrl.getEventsById);
    app.route(config.api_version + '/events/:id').put(eventCtrl.updateEvents);
    app.route(config.api_version + '/events/:id').delete(eventCtrl.deleteEvents);

};
