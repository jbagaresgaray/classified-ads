'use strict';

var industryCtrl = require('../controllers/industrycontroller');

module.exports = function(app, config) {

    app.route(config.api_version + '/industry/:id').get(industryCtrl.getIndustryByID)
    app.route(config.api_version + '/industry').get(industryCtrl.getIndustry)
    app.route(config.api_version + '/industry').post(industryCtrl.createIndustry)
    app.route(config.api_version + '/industry/:id').put(industryCtrl.updateIndustry)
    app.route(config.api_version + '/industry/:id').delete(industryCtrl.deleteIndustry);

};
