'use strict';

var restaurtantCtrl = require('../controllers/restaurantcontroller');

module.exports = function(app, config) {

    app.route(config.api_version + '/restaurant/:id').get(restaurtantCtrl.getRestaurantByID)
    app.route(config.api_version + '/restaurant').get(restaurtantCtrl.getRestaurant)
    app.route(config.api_version + '/restaurant').post(restaurtantCtrl.createRestaurant)
    app.route(config.api_version + '/restaurant/:id').put(restaurtantCtrl.updateRestaurant)
    app.route(config.api_version + '/restaurant/:id').delete(restaurtantCtrl.deleteRestaurant);

};
