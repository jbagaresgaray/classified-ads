'use strict';

var uploadCtrl = require('../controllers/uploadcontroller');

module.exports = function(app, config) {

    app.route(config.api_version + '/upload').post(uploadCtrl.uploadLogo);

};
