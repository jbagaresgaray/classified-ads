'use strict';

var homephotoCtrl = require('../controllers/homephotocontroller');

module.exports = function(app, config) {

    app.route(config.api_version + '/homephoto').get(homephotoCtrl.getHomePhoto);
    app.route(config.api_version + '/homephoto').post(homephotoCtrl.createHomePhoto);
    app.route(config.api_version + '/homephoto/:id').get(homephotoCtrl.getHomePhotosById);
    app.route(config.api_version + '/homephoto/:id').put(homephotoCtrl.updateHomePhoto);
    app.route(config.api_version + '/homephoto/:id').delete(homephotoCtrl.deleteHomePhoto);

};
