'use strict';

var propertyCtrl = require('../controllers/propertycontroller');

module.exports = function(app, config) {

    app.route(config.api_version + '/property/:id').get(propertyCtrl.getPropertyByID)
    app.route(config.api_version + '/property').get(propertyCtrl.getProperty)
    app.route(config.api_version + '/property').post(propertyCtrl.createProperty)
    app.route(config.api_version + '/property/:id').put(propertyCtrl.updateProperty)
    app.route(config.api_version + '/property/:id').delete(propertyCtrl.deleteProperty);

};
