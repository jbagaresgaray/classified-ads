'use strict';

var cb = require('./../utils/callback');
var homephotoservices = require('../services/homephotoservices').HomePhoto;
var homephoto = new homephotoservices();



exports.getHomePhotosById = function(req, res) {
    homephoto.getHomePhotosById(req.params.id,cb.setupResponseCallback(res));
};

exports.getHomePhoto= function(req, res) {
    homephoto.getHomePhoto(cb.setupResponseCallback(res));
};

exports.createHomePhoto = function(req, res) {
    homephoto.createHomePhoto(req.body,cb.setupResponseCallback(res));
};

exports.updateHomePhoto = function(req, res) {
    homephoto.updateHomePhoto(req.params.id,req.body,cb.setupResponseCallback(res));
};

exports.deleteHomePhoto = function(req, res) {
    homephoto.deleteHomePhoto(req.params.id,cb.setupResponseCallback(res));
};
