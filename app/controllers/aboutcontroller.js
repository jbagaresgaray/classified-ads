'use strict';

var cb = require('./../utils/callback');
var aboutservices = require('../services/aboutservices').About;
var about = new aboutservices();



exports.getAbout = function(req, res) {
    about.getAbout(cb.setupResponseCallback(res));
};

exports.createAbout = function(req, res) {
	about.createAbout(req.body,cb.setupResponseCallback(res));
};

exports.updateAbout = function(req, res) {
	var storage = multer.diskStorage({
        destination: function(req, file, cb) {
            var dir = 'public/uploads';
            mkdirp(dir, function(err) {
                cb(err, dir);
            });
        },
        filename: function(req, file, cb) {
            cb(null, Math.floor(Date.now() / 1000) + '_' + file.originalname);
        }
    });

    var limits = { fileSize: 1024 * 1024 * 1024 };

    var fileFilter = function(req, file, cb) {

        if ((file.mimetype !== 'image/png') && 
            (file.mimetype !== 'image/jpeg') && 
            (file.mimetype !== 'image/jpg') && 
            (file.mimetype !== 'image/gif') && 
            (file.mimetype !== 'image/x-ms-bmp')) {
            req.fileValidationError = 'Invalid file type. Must be .png, .jpg, .jpeg, .gif, .bmp';
            return cb(null, false, new Error('Invalid file type. Must be .png, .jpg, .jpeg, .gif, .bmp'));
        }

        cb(null, true);
    };

    var uploadImg = multer({
        storage: storage,
        fileFilter: fileFilter,
        limits: limits
    }).single('file_image');

    uploadImg(req, res, function(err) {
        if (err) {
            return res.status(500).json({
                response: err,
                statusCode: 500
            });
        }

        if(req.fileValidationError) {
            return res.status(400).json({
                response: {
                    result: null,
                    msg: req.fileValidationError,
                    success: false
                },
                statusCode: 400
            });
        }

        if (req.file) {
            req.body.headerimage = req.file.destination + '/' + req.file.filename;
        } else {
            req.body.headerimage = '';
        }

        about.updateAbout(req.params.id,req.body,cb.setupResponseCallback(res));
    });
};

exports.deleteAbout = function(req, res) {
	console.log('ID: ',req.params.id);
    // about.deleteAbout(req.params.id,cb.setupResponseCallback(res));
};
