'use strict';

var cb = require('./../utils/callback');
var multer = require('multer');
var path = require('path');
var mkdirp = require('mkdirp');

var uploadservices = require('../services/uploadservices');


exports.uploadLogo = function(req, res, next) {
	var storage = multer.diskStorage({
        destination: function(req, file, cb) {
            var dir = 'public/uploads';
            mkdirp(dir, function(err) {
                cb(err, dir);
            });
        },
        filename: function(req, file, cb) {
            cb(null, Math.floor(Date.now() / 1000) + '_' + file.originalname);
        }
    });

    var limits = { fileSize: 1024 * 1024 * 1024 };

    var fileFilter = function(req, file, cb) {

    	console.log('req.fileFilter.body : ',req.body);

        if ((file.mimetype !== 'image/png') && 
            (file.mimetype !== 'image/jpeg') && 
            (file.mimetype !== 'image/jpg') && 
            (file.mimetype !== 'image/gif') && 
            (file.mimetype !== 'image/x-ms-bmp')) {
            req.fileValidationError = 'Invalid file type. Must be .png, .jpg, .jpeg, .gif, .bmp';
            return cb(null, false, new Error('Invalid file type. Must be .png, .jpg, .jpeg, .gif, .bmp'));
        }

        cb(null, true);
    };


    var uploadImg = multer({
        storage: storage,
        fileFilter: fileFilter,
        limits: limits
    }).single('file_image');

    uploadImg(req, res, function(err) {
    	console.log('req.body 1: ',req.body);

        if (err) {
            return res.status(500).json({
                response: err,
                statusCode: 500
            });
        }

        if(req.fileValidationError) {
            return res.status(400).json({
                response: {
                    result: null,
                    msg: req.fileValidationError,
                    success: false
                },
                statusCode: 400
            });
        }

        console.log('req.file: ',req.file);
        if (req.file) {
            req.body.headerimage = req.file.destination + '/' + req.file.filename;
        } else {
            req.body.headerimage = '';
        }
        console.log('req.body: ',req.body);
        // about.createAbout(req.body,cb.setupResponseCallback(res));
    });
    
    uploadservices.uploadLogo(files, cb.setupResponseCallback(res));
};
