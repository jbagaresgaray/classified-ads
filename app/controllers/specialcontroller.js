'use strict';

var cb = require('./../utils/callback');
var specialsservices = require('../services/specialsservices').Specials;
var specials = new specialsservices();



exports.getSpecialsByID = function(req, res) {
    specials.getSpecialsByID(req.params.id,cb.setupResponseCallback(res));
};

exports.getSpecials = function(req, res) {
    specials.getSpecials(cb.setupResponseCallback(res));
};

exports.createSpecials = function(req, res) {
    specials.createSpecials(req.body,cb.setupResponseCallback(res));
};

exports.updateSpecials = function(req, res) {
    specials.updateSpecials(req.params.id,req.body,cb.setupResponseCallback(res));
};

exports.deleteSpecials = function(req, res) {
    specials.deleteSpecials(req.params.id,cb.setupResponseCallback(res));
};
