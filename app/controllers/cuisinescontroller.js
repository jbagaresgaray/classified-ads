'use strict';

var cb = require('./../utils/callback');
var cuisinesservices = require('../services/cuisinesservices').Cuisines;
var cuisine = new cuisinesservices();



exports.getCuisinesById = function(req, res) {
    cuisine.getCuisinesById(req.params.id,cb.setupResponseCallback(res));
};

exports.getCuisines = function(req,res){
	cuisine.getCuisines(cb.setupResponseCallback(res));
};

exports.createCuisines = function(req, res) {
    cuisine.createCuisines(req.body,cb.setupResponseCallback(res));
};

exports.updateCuisines = function(req, res) {
    cuisine.updateCuisines(req.params.id,req.body,cb.setupResponseCallback(res));
};

exports.deleteCuisines = function(req, res) {
    cuisine.deleteCuisines(req.params.id,cb.setupResponseCallback(res));
};
