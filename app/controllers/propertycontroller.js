'use strict';

var cb = require('./../utils/callback');
var propertyservices = require('../services/propertyservices').Property;
var property = new propertyservices();



exports.getPropertyByID = function(req, res) {
    property.getPropertyByID(req.params.id,cb.setupResponseCallback(res));
};

exports.getProperty = function(req, res) {
    property.getProperty(cb.setupResponseCallback(res));
};

exports.createProperty = function(req, res) {
    property.createProperty(req.body,cb.setupResponseCallback(res));
};

exports.updateProperty = function(req, res) {
    property.updateProperty(req.params.id,req.body,cb.setupResponseCallback(res));
};

exports.deleteProperty = function(req, res) {
    property.deleteProperty(req.params.id,cb.setupResponseCallback(res));
};
