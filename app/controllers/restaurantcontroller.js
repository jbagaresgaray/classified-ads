'use strict';

var cb = require('./../utils/callback');
var restaurantservices = require('../services/restaurantservices').Restaurant;
var restaurant = new restaurantservices();



exports.getRestaurantByID = function(req, res) {
    restaurant.getRestaurantByID(req.params.id,cb.setupResponseCallback(res));
};

exports.getRestaurant = function(req, res) {
    restaurant.getRestaurant(cb.setupResponseCallback(res));
};

exports.createRestaurant = function(req, res) {
    restaurant.createRestaurant(req.body,cb.setupResponseCallback(res));
};

exports.updateRestaurant = function(req, res) {
    restaurant.updateRestaurant(req.params.id,req.body,cb.setupResponseCallback(res));
};

exports.deleteRestaurant = function(req, res) {
    restaurant.deleteRestaurant(req.params.id,cb.setupResponseCallback(res));
};
