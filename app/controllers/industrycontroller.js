'use strict';

var cb = require('./../utils/callback');
var industryservices = require('../services/industryservices').Industry;
var industry = new industryservices();



exports.getIndustryByID = function(req, res) {
    industry.getIndustryByID(req.params.id,cb.setupResponseCallback(res));
};

exports.getIndustry = function(req, res) {
    industry.getIndustry(cb.setupResponseCallback(res));
};

exports.createIndustry = function(req, res) {
    industry.createIndustry(req.body,cb.setupResponseCallback(res));
};

exports.updateIndustry = function(req, res) {
    industry.updateIndustry(req.params.id,req.body,cb.setupResponseCallback(res));
};

exports.deleteIndustry = function(req, res) {
    industry.deleteIndustry(req.params.id,cb.setupResponseCallback(res));
};
