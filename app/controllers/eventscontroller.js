'use strict';

var cb = require('./../utils/callback');
var eventsservices = require('../services/eventsservices').Events;
var events = new eventsservices();



exports.getEventsById = function(req, res) {
    events.getEventsById(req.params.id,cb.setupResponseCallback(res));
};

exports.getEvents= function(req, res) {
    events.getEvents(cb.setupResponseCallback(res));
};

exports.createEvents = function(req, res) {
    events.createEvents(req.body,cb.setupResponseCallback(res));
};

exports.updateEvents = function(req, res) {
    events.updateEvents(req.params.id,req.body,cb.setupResponseCallback(res));
};

exports.deleteEvents = function(req, res) {
    events.deleteEvents(req.params.id,cb.setupResponseCallback(res));
};
