'use strict';

var cb = require('./../utils/callback');
var businessservices = require('../services/businessservices').Business;
var business = new businessservices();



exports.getBusinessById = function(req, res) {
    business.getBusinessById(req.params.id,cb.setupResponseCallback(res));
};

exports.getBusiness = function(req,res){
	business.getBusiness(cb.setupResponseCallback(res));
};

exports.createBusiness = function(req, res) {
    business.createBusiness(req.body,cb.setupResponseCallback(res));
};

exports.updateBusiness = function(req, res) {
    business.updateBusiness(req.params.id,req.body,cb.setupResponseCallback(res));
};

exports.deleteBusiness = function(req, res) {
    business.deleteBusiness(req.params.id,cb.setupResponseCallback(res));
};
