'use strict';

module.exports = function(mongoose) {
    var IndustrySchema = new mongoose.Schema({
        title: String,
        slug: String,
        description: String,
        isDraft: Number,
        isPublish:Number,
        datePublish: Date,
        dateCreated: {
            type: Date,
            default: Date.now
        }/*,
        createdBy:{
			type: mongoose.Schema.Types.ObjectId,
			ref: 'Users'
		},
		dateUpdated:Date,
		updatedBy:{
			type: mongoose.Schema.Types.ObjectId,
			ref: 'Users'
		}*/
    });

    mongoose.model('Industry', IndustrySchema);
};
