'use strict';

module.exports = function(mongoose) {
    var HomeSchema = new mongoose.Schema({
        title: String,
        photo: String,
        description: String,
        isDraft: Number,
        isPublish:Number,
        datePublish: Date,
        dateCreated: {
            type: Date,
            default: Date.now
        }/*,
        createdBy:{
			type: mongoose.Schema.Types.ObjectId,
			ref: 'Users'
		},
		dateUpdated:Date,
		updatedBy:{
			type: mongoose.Schema.Types.ObjectId,
			ref: 'Users'
		}*/
    });

    mongoose.model('HomePhotos', HomeSchema);
};
