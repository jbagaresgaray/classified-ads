'use strict';

module.exports = function(mongoose) {
    var PropertySchema = new mongoose.Schema({
        title: String,
        streetNumber: String,
        streetName: String,
        propertyDescription: String,
        address: String,
        cv: String,
        soldDate:Date,
        salePrice:Number,
        photo:String,
        webLink:String,
        agentName: String,
        agentEmail: String,
        agentPhone: Number,
        landArea: String,
        floorArea: String,
        bedRooms: String,
        propertyState: String,
        isDraft: Number,
        isPublish:Number,
        datePublish: Date,
        dateCreated: {
            type: Date,
            default: Date.now
        }/*,
        createdBy:{
			type: mongoose.Schema.Types.ObjectId,
			ref: 'Users'
		},
		dateUpdated:Date,
		updatedBy:{
			type: mongoose.Schema.Types.ObjectId,
			ref: 'Users'
		}*/
    });

    mongoose.model('Property', PropertySchema);
};
