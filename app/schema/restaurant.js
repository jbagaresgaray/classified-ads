'use strict';

module.exports = function(mongoose) {
    var RestaurantSchema = new mongoose.Schema({
        title: String,
        description: String,
        address: String,
        location: String,
        openingHours: String,
        phoneNumber: Number,
        photo: String,
        emailAddress: String,
        webSite: String,
        cuisines:[],
        isDraft: Number,
        isPublish:Number,
        datePublish: Date,
        dateCreated: {
            type: Date,
            default: Date.now
        }/*,
        createdBy:{
            type: mongoose.Schema.Types.ObjectId,
            ref: 'Users'
        },
        dateUpdated:Date,
        updatedBy:{
            type: mongoose.Schema.Types.ObjectId,
            ref: 'Users'
        }*/
    });

    mongoose.model('Restaurant', RestaurantSchema);
};
