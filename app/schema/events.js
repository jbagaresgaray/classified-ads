'use strict';

module.exports = function(mongoose) {
    var EventsSchema = new mongoose.Schema({
        title: String,
        description: String,
        eventDate: Date,
        location: String,
        eventImage: String,
        isDraft: Number,
        isPublish:Number,
        datePublish: Date,
        dateCreated: {
            type: Date,
            default: Date.now
        }/*,
        createdBy:{
            type: mongoose.Schema.Types.ObjectId,
            ref: 'Users'
        },
        dateUpdated:Date,
        updatedBy:{
            type: mongoose.Schema.Types.ObjectId,
            ref: 'Users'
        }*/
    });

    mongoose.model('Events', EventsSchema);
};
