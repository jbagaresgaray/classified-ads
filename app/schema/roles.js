'use strict';

module.exports = function(mongoose) {
    var RoleSchema = new mongoose.Schema({
        RoleType: String,
        RoleDesc: String
    });
    mongoose.model('Roles', RoleSchema);
};
