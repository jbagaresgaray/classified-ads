'use strict';

module.exports = function(mongoose) {
    var SpecialsSchema = new mongoose.Schema({
        title: String,
        description: String,
        businessName:{
            type: mongoose.Schema.Types.ObjectId,
            ref: 'Business'
        },
        isDraft: Number,
        isPublish:Number,
        datePublish: Date,
        dateCreated: {
            type: Date,
            default: Date.now
        }/*,
        createdBy:{
            type: mongoose.Schema.Types.ObjectId,
            ref: 'Users'
        },
        dateUpdated:Date,
        updatedBy:{
            type: mongoose.Schema.Types.ObjectId,
            ref: 'Users'
        }*/
    });

    mongoose.model('Specials', SpecialsSchema);
};
