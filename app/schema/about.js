'use strict';

module.exports = function(mongoose) {
    var AboutSchema = new mongoose.Schema({
        headerImage:  {
            file_name: String,
            file_type: String,
            url: String,
            file_size: Number
        },
        section1: String,
        section2: String,
        section3: String,
        section4: String,
        section5: String,
        isDraft: Number,
        isPublish:Number,
        datePublish: Date,
        dateCreated: {
            type: Date,
            default: Date.now
        }/*,
        createdBy:{
            type: mongoose.Schema.Types.ObjectId,
            ref: 'Users'
        },
        dateUpdated:Date,
        updatedBy:{
            type: mongoose.Schema.Types.ObjectId,
            ref: 'Users'
        }*/
    });

    mongoose.model('About', AboutSchema);
};
