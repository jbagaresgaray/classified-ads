'use strict';

module.exports = function(mongoose) {
    var BusinessSchema = new mongoose.Schema({
        title: String,
        description: String,
        address: String,
        location: String,
        openingHours: String,
        phoneNumber: Number,
        photo: {
            file_name: String,
            file_type: String,
            url: String,
            file_size: Number
        },
        emailAddress: String,
        webSite: String,
        industry: [],
        isDraft: Number,
        isPublish: Number,
        datePublish: Date,
        dateCreated: {
            type: Date,
            default: Date.now
        }
        /*,
                createdBy:{
                    type: mongoose.Schema.Types.ObjectId,
                    ref: 'Users'
                },
                dateUpdated:Date,
                updatedBy:{
                    type: mongoose.Schema.Types.ObjectId,
                    ref: 'Users'
                }*/
    });

    mongoose.model('Business', BusinessSchema);
};
