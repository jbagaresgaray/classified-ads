'use strict';

var express = require('express');
var router = express.Router({
    strict: true,
    caseSensitive: true
});

var env = process.env.NODE_ENV;
var config = require('../../config/environment/' + env);

router.all('/home/*', function(req, res) {
	/*if (req.isAuthenticated()) {
        res.render('index');
    } else {
        res.redirect('/login');
    }*/
    res.render('home');
});

router.all(config.api_version + '/*', function(req, res, next) {
    /*if (!req.isAuthenticated()) {
        res.status(401).json({});
    } else {
        next();
    }*/
     next();
});

exports.router = router;
