'use strict';

exports.validateAbout = function(req, res, next) {
    req.checkBody('headerimage', 'Please provide Header Image path').notEmpty();
    req.checkBody('label', 'Please provide Attribute Label').notEmpty();
    req.checkBody('attrib_type_id', 'Please provide Attribute Type').notEmpty();

    var errors = req.validationErrors();

    if (errors) {
        res.status(400).send({
            response: {
                result: errors,
                msg: '',
                success: false
            },
            statusCode: 400
        });
    } else {
        next();
    }
};
