'use strict';

var eventsdaos = require('../daos/eventsdaos');

function Events() {
    this.eventsdaos = eventsdaos;
}

Events.prototype.createEvents = function(data, next) {
    eventsdaos.createEvents(data, next);
};

Events.prototype.getEvents = function(next){
	eventsdaos.getEvents(next);
};

Events.prototype.getEventsById = function(id,next){
	eventsdaos.getEventsById(id,next);
};

Events.prototype.updateEvents = function(id,data,next){
	eventsdaos.updateEvents(id,data,next);
};

Events.prototype.deleteEvents = function(id,next){
	eventsdaos.deleteEvents(id,next);
};

exports.Events = Events;
