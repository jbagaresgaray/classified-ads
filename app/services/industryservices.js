'use strict';

var industrydaos = require('../daos/industrydaos');

function Industry() {
    this.industrydaos = industrydaos;
}

Industry.prototype.createIndustry = function(data, next) {
    industrydaos.createIndustry(data, next);
};

Industry.prototype.getIndustry = function(next){
	industrydaos.getIndustry(next);
};

Industry.prototype.getIndustryByID = function(id,next){
	industrydaos.getIndustryByID(id,next);
};

Industry.prototype.updateIndustry = function(id,data,next){
	industrydaos.updateIndustry(id,data,next);
};

Industry.prototype.deleteIndustry = function(id,next){
	industrydaos.deleteIndustry(id,next);
};

exports.Industry = Industry;
