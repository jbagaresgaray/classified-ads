'use strict';

var propertydaos = require('../daos/propertydaos');

function Property() {
    this.propertydaos = propertydaos;
}

Property.prototype.createProperty = function(data, next) {
    propertydaos.createProperty(data, next);
};

Property.prototype.getProperty = function(next){
	propertydaos.getProperty(next);
};

Property.prototype.getPropertyByID = function(id,next){
	propertydaos.getPropertyByID(id,next);
};

Property.prototype.updateProperty = function(id,data,next){
	propertydaos.updateProperty(id,data,next);
};

Property.prototype.deleteProperty = function(id,next){
	propertydaos.deleteProperty(id,next);
};

exports.Property = Property;
