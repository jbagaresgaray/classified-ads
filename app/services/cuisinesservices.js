'use strict';

var cuisinesdaos = require('../daos/cuisinesdaos');

function Cuisines() {
    this.cuisinesdaos = cuisinesdaos;
}

Cuisines.prototype.createCuisines = function(data, next) {
    cuisinesdaos.createCuisines(data, next);
};

Cuisines.prototype.getCuisines = function(next){
	cuisinesdaos.getCuisines(next);
};

Cuisines.prototype.getCuisinesById = function(id,next){
	cuisinesdaos.getCuisinesById(id,next);
};

Cuisines.prototype.updateCuisines = function(id,data,next){
	cuisinesdaos.updateCuisines(id,data,next);
};

Cuisines.prototype.deleteCuisines = function(id,next){
	cuisinesdaos.deleteCuisines(id,next);
};

exports.Cuisines = Cuisines;
