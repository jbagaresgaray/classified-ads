'use strict';
var fs = require('fs-extra');
var path = require('path');

exports.uploadLogo = function(files, next) {
    var u_image = {}; // declare u_image object
    var data = {}; // declare data object to push from u_image object

    if (files) {

        var targetPath = path.resolve('./public/uploads/' + Date.now() + '-' + files.name); // locate the target Path to save the image.
        data.file_name = files.name;
        data.file_type = files.mimetype;
        data.url = 'public/uploads/' + Date.now() + '-' + files.name;
        data.file_size = files.size;
        u_image.u_image = data;

        if (path.extname(files.originalname).toLowerCase() === '.png' || path.extname(files.originalname).toLowerCase() === '.jpg' || path.extname(files.originalname).toLowerCase() === '.jpeg' || path.extname(files.originalname).toLowerCase() === '.gif') {
            fs.copy(files.path, targetPath, function(err) {
                if (err) {
                    next({
                        response_code: 415,
                        success: false,
                        result: err,
                        msg: 'invalid photo '
                    }, null);
                }

                next(null, {
                    response_code: 200,
                    success: true,
                    result: u_image.u_image,
                    msg: 'Successfully Uploaded'
                });
            });
        } else {
            next(null, {
                response_code: 415,
                success: false,
                result: '',
                msg: 'invalid photo '
            });
        }
    } else {
        next(null, {
            response_code: 200,
            success: true,
            result: null,
            msg: 'no photo '
        });

    }

};
