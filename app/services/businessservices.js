'use strict';

var businessdaos = require('../daos/businessdaos');

function Business() {
    this.businessdaos = businessdaos;
}

Business.prototype.createBusiness = function(data, next) {
    businessdaos.createBusiness(data, next);
};

Business.prototype.getBusinessById = function(id,next){
	businessdaos.getBusinessById(id,next);
};

Business.prototype.getBusiness = function(next){
	businessdaos.getBusiness(next);
};

Business.prototype.updateBusiness = function(id,data,next){
	businessdaos.updateBusiness(id,data,next);
};

Business.prototype.deleteBusiness = function(id,next){
	businessdaos.deleteBusiness(id,next);
};

exports.Business = Business;
