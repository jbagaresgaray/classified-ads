'use strict';

var aboutdaos = require('../daos/aboutdaos');

function About() {
    this.aboutdaos = aboutdaos;
}

About.prototype.createAbout = function(data, next) {
    aboutdaos.createAbout(data, next);
};

About.prototype.getAbout = function(next){
	aboutdaos.getAbout(next);
};

About.prototype.updateAbout = function(id,data,next){
	aboutdaos.updateAbout(id,data,next);
};

About.prototype.deleteAbout = function(id,next){
	console.log('ID: ',id);
	aboutdaos.deleteAbout(id,next);
};

exports.About = About;
