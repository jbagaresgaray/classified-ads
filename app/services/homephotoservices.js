'use strict';

var homephotodaos = require('../daos/homephotodaos');

function HomePhoto() {
    this.homephotodaos = homephotodaos;
}

HomePhoto.prototype.createHomePhoto = function(data, next) {
    homephotodaos.createHomePhoto(data, next);
};

HomePhoto.prototype.getHomePhoto = function(next){
	homephotodaos.getHomePhoto(next);
};

HomePhoto.prototype.getHomePhotosById = function(id,next){
	homephotodaos.getHomePhotosById(id,next);
};

HomePhoto.prototype.updateHomePhoto = function(id,data,next){
	homephotodaos.updateHomePhoto(id,data,next);
};

HomePhoto.prototype.deleteHomePhoto = function(id,next){
	homephotodaos.deleteHomePhoto(id,next);
};

exports.HomePhoto = HomePhoto;