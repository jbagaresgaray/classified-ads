'use strict';

var specialsdaos = require('../daos/specialsdaos');

function Specials() {
    this.specialsdaos = specialsdaos;
}

Specials.prototype.createSpecials = function(data, next) {
    specialsdaos.createSpecials(data, next);
};

Specials.prototype.getSpecials = function(next){
	specialsdaos.getSpecials(next);
};

Specials.prototype.getSpecialsByID = function(id,next){
	specialsdaos.getSpecialsByID(id,next);
};

Specials.prototype.updateSpecials = function(id,data,next){
	specialsdaos.updateSpecials(id,data,next);
};

Specials.prototype.deleteSpecials = function(id,next){
	specialsdaos.deleteSpecials(id,next);
};

exports.Specials = Specials;
