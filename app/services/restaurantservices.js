'use strict';

var restaurantdaos = require('../daos/restaurantdaos');

function Restaurant() {
    this.restaurantdaos = restaurantdaos;
}

Restaurant.prototype.createRestaurant = function(data, next) {
    restaurantdaos.createRestaurant(data, next);
};

Restaurant.prototype.getRestaurant = function(next){
	restaurantdaos.getRestaurant(next);
};

Restaurant.prototype.getRestaurantByID = function(id,next){
	restaurantdaos.getRestaurantByID(id,next);
};

Restaurant.prototype.updateRestaurant = function(id,data,next){
	restaurantdaos.updateRestaurant(id,data,next);
};

Restaurant.prototype.deleteRestaurant = function(id,next){
	restaurantdaos.deleteRestaurant(id,next);
};

exports.Restaurant = Restaurant;
