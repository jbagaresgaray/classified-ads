"use strict";

module.exports = {
  env: process.env.NODE_ENV,
  dbUrl: process.env.DB_HOST,
  db_user: process.env.DB_USER,
  db_password: process.env.DB_PASS,
  db_port: process.env.DB_PORT,
  port: process.env.PORT || process.env.APP_PORT || 3000,
  ip: process.env.IP,
  socket_port: process.env.SOCKET_PORT,
  app_name: process.env.APP_NAME,
  api_host_url: process.env.API_HOST_URL,
  frontend_host_url: process.env.FRONTEND_HOST_URL,
  api_version: process.env.API_VERSION
};
